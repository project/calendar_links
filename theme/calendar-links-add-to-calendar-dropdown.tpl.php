<?php

/**
 * @file
 * Default theme implementation for add to calendar dropdown.
 *
 * Available variables:
 * - $outlook: Outlook add to calendar link.
 * - $ical: iCal add to calendar link.
 * - $google: Google add to calendar link.
 * - $yahoo: Yahoo add to calendar link.
 */
?>

<div class="calendar-links-add-to-calendar-dropdown">
  <form>
    <select name="add-to-calendar" class="form-control form-select" onchange="(this.value !== '') ? location = this.value : false;">
        <option value="">Add To Calendar</option>
      <?php if ($outlook): ?>
        <option value="<?php print $outlook; ?>">Outlook</option>
      <?php endif; ?>
      <?php if ($ical): ?>
        <option value="<?php print $ical; ?>">iCal</option>
      <?php endif; ?>
      <?php if ($google): ?>
        <option value="<?php print $google; ?>">Google</option>
      <?php endif; ?>
      <?php if ($yahoo): ?>
        <option value="<?php print $yahoo; ?>">Yahoo</option>
      <?php endif; ?>
    </select>
  </form>
</div>
