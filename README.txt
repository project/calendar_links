CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Maintainers

INTRODUCTION
------------

The Calendar links module provides links for Adding to Your Calendar for the
following: iCal, Outlook, Google, and Yahoo.  This module is great for cases
where a custom module needs to add the calendar links.  It borrows a lot from
the Add To Cal module but does not require any entities.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

MAINTAINERS
-----------

Current maintainers:
 * Albert Jankowski (albertski) - https://www.drupal.org/u/albertski
